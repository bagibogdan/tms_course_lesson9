﻿using System;

namespace Shop
{
    public interface IEdible
    {
        bool IsEdible { get; }
    }

    public interface IVegetarian
    {
        bool IsVegetarian { get; }
    }

    public interface IWooden
    {
        bool IsWooded { get; }
    }

    public interface IClerical
    {
        bool IsClerical { get; }
    }
}
