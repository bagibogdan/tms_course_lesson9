﻿using System;

namespace Shop
{
    public class Sausage : Edible
    {
        public Sausage()
        {
            _name = "Сосиски";
            _price = 7.5f;
            _amount = 9;
            _calories = 266;
        }
    }
}
