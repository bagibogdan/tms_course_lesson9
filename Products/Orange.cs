﻿using System;

namespace Shop
{
    public class Orange : Vegetarian
    {
        public Orange()
        {
            _name = "Апельсин";
            _price = 6.5f;
            _amount = 3;
            _calories = 36;
        }
    }
}
