﻿using System;

namespace Shop
{
    public class Pen : Clerical
    {
        public Pen()
        {
            _name = "Ручка";
            _price = 0.87f;
            _amount = 10;
            _material = "пластик";
        }
    }
}
