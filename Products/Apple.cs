﻿using System;

namespace Shop
{
    public class Apple : Vegetarian
    {
        public Apple()
        {
            _name = "Яблоко";
            _price = 3.5f;
            _amount = 5;
            _calories = 47;
        }
    }
}
