﻿using System;

namespace Shop
{
    public class Carrot : Vegetarian
    {
        public Carrot()
        {
            _name = "Морковь";
            _price = 5.2f;
            _amount = 10;
            _calories = 32;
        }
    }
}
