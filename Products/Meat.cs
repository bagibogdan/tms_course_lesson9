﻿using System;

namespace Shop
{
    public class Meat : Edible
    {
        public Meat()
        {
            _name = "Мясо";
            _price = 12.5f;
            _amount = 15;
            _calories = 259;
        }
    }
}
