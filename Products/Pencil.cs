﻿using System;

namespace Shop
{
    public class Pencil : Clerical, IWooden
    {
        public bool IsWooded => true;

        public Pencil()
        {
            _name = "Карандаш";
            _price = 0.53f;
            _amount = 20;
            _material = "дерево";
        }
    }
}
