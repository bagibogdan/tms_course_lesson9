﻿using System;

namespace Shop
{
    public class Cup : Inedible
    {
        public Cup()
        {
            _name = "Чашка";
            _price = 7.95f;
            _amount = 7;
            _material = "форфор";
        }
    }
}
