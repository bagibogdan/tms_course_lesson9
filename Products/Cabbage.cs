﻿using System;

namespace Shop
{
    public class Cabbage : Vegetarian
    {
        public Cabbage()
        {
            _name = "Капуста";
            _price = 6.3f;
            _amount = 7;
            _calories = 27;
        }
    }
}
