﻿using System;

namespace Shop
{
    public class Table : Inedible, IWooden
    {
        public bool IsWooded => true;

        public Table()
        {
            _name = "Стол";
            _price = 50.7f;
            _amount = 3;
            _material = "дерево";
        }
    }
}
