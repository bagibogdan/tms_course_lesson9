﻿using System;

namespace Shop
{
    class Program
    {
        static void Main(string[] args)
        {
            Shop shop = new Shop("ДИСКОНТ", "ул. Много-Денег д.99");

            shop.AddProduct(new Apple());
            shop.AddProduct(new Orange());
            shop.AddProduct(new Pencil());
            shop.AddProduct(new Meat());
            shop.AddProduct(new Pen());

            shop.ShowAllProducts();
            shop.ShowProductsPrices();

            shop.AddProduct(new Sausage());
            shop.AddProduct(new Cabbage());
            shop.AddProduct(new Table());
            shop.AddProduct(new Carrot());
            shop.AddProduct(new Cup());

            shop.ShowAllProducts();
            shop.ShowProductsPrices();

            shop.ShowEdibleProducts();

            shop.ShowVegetarianProducts();

            shop.ShowNonvegetarianProducts();

            shop.ShowInedibleProducts();

            shop.ShowWoodenProducts();

            shop.ShowClericalProducts();

            shop.DeleteProduct("Яблоко");
            shop.DeleteProduct("Стол");

            shop.ShowAllProducts();

            Console.ReadKey();
        }
    }
}
