﻿using System;
using System.Collections.Generic;

namespace Shop
{
    public class Shop
    {
        private string _name;
        private string _address;
        public List<Product> Products = new List<Product>();

        public string Name => _name;
        public string Address => _address;

        public Shop()
        {
            _name = "Название";
            _address = "Адрес";
        }

        public Shop(string name, string address)
        {
            _name = name;
            _address = address;
        }

        public void AddProduct(Product product)
        {
            Products.Add(product);
            Console.WriteLine($"Товар {product.Name} добавлен в список товаров магазина {_name} по адресу {_address}.");
        }

        public void DeleteProduct(string name)
        {
            foreach (Product product in Products)
            {
                if (product.Name == name)
                {
                    Console.WriteLine($"Товар {product.Name} удален из списка товаров.");
                    Products.Remove(product);
                    Console.WriteLine();
                    return;
                }
            }
        }

        public void ShowAllProducts()
        {
            int count = 0;

            Console.WriteLine();
            Console.WriteLine($"Список всех товаров магазина {_name}:");

            foreach (Product product in Products)
            {
                Console.Write($"{count++ + 1}. ");
                product.ShowProductInfo();
                Console.WriteLine();
            }

            Console.WriteLine();
        }

        public void ShowProductsPrices()
        {
            Product temp;
            int count = 0;

            Console.WriteLine($"Список всех товаров в магазине {_name}, отсортированный по возрастанию цены:");

            for (int i = 0; i < Products.Count - 1; i++)
            {
                for (int j = i + 1; j < Products.Count; j++)
                {
                    if (Products[i].Price > Products[j].Price)
                    {
                        temp = Products[i];
                        Products[i] = Products[j];
                        Products[j] = temp;
                    }
                }
            }

            foreach (Product product in Products)
            {
                Console.Write($"{count++ + 1}. ");
                product.ShowProductInfo();
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public void ShowEdibleProducts()
        {
            int count = 0;

            Console.WriteLine($"Список съедобных товаров в магазине {_name}:");

            foreach (Product product in Products)
            {
                if (product is Edible)
                {
                    Console.Write($"{count++ + 1}. ");
                    Console.WriteLine(product);
                }
            }

            Console.WriteLine();
        }

        public void ShowInedibleProducts()
        {
            int count = 0;

            Console.WriteLine($"Список несъедобных товаров в магазине {_name}:");

            foreach (Product product in Products)
            {
                if (product is Inedible)
                {
                    Console.Write($"{count++ + 1}. ");
                    Console.WriteLine(product);
                }
            }

            Console.WriteLine();
        }

        public void ShowVegetarianProducts()
        {
            int count = 0;

            Console.WriteLine($"Список вегетарианских товаров в магазине {_name}:");
            foreach (Product product in Products)
            {
                if (product is Edible && product is Vegetarian)
                {
                    Console.Write($"{count++ + 1}. ");
                    Console.WriteLine(product);
                }
            }
            Console.WriteLine();
        }

        public void ShowNonvegetarianProducts()
        {
            int count = 0;

            Console.WriteLine($"Список невегетарианских товаров в магазине {_name}:");

            foreach (Product product in Products)
            {
                if (product is Edible && !(product is Vegetarian))
                {
                    Console.Write($"{count++ + 1}. ");
                    Console.WriteLine(product);
                }
            }

            Console.WriteLine();
        }

        public void ShowWoodenProducts()
        {
            int count = 0;

            Console.WriteLine($"Список деревянных товаров в магазине {_name}:");

            foreach (Product product in Products)
            {
                if (product is Inedible && product is IWooden)
                {
                    Console.Write($"{count++ + 1}. ");
                    Console.WriteLine(product);
                }
            }

            Console.WriteLine();
        }

        public void ShowClericalProducts()
        {
            int count = 0;

            Console.WriteLine($"Список канцелярских товаров в магазине {_name}:");

            foreach (Product product in Products)
            {
                if (product is Inedible && product is Clerical)
                {
                    Console.Write($"{count++ + 1}. ");
                    Console.WriteLine(product);
                }
            }

            Console.WriteLine();
        }
    }
}
