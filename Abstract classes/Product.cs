﻿using System;

namespace Shop
{
    public abstract class Product
    {
        protected string _name;
        protected float _price;
        protected int _amount;
        protected string _measure;

        public string Name => _name;
        public float Price => _price;

        public Product()
        {
            _name = "Product";
            _price = 0;
            _amount = 0;
            _measure = "";
        }

        public virtual void ShowProductInfo()
        {
            Console.Write($"{_name}, цена: {_price} руб., кол-во на складе: {_amount} {_measure}");
        }

        public override string ToString()
        {
            return _name;
        }
    }
}
