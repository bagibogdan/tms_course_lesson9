﻿using System;

namespace Shop
{
    public abstract class Edible : Product, IEdible
    {
        protected float _calories;

        public bool IsEdible => true;

        public Edible()
        {
            _measure = "кг";
        }

        public override void ShowProductInfo()
        {
            base.ShowProductInfo();
            Console.Write($", калорийность на 100 г: {_calories} ккал.");
        }
    }
}
