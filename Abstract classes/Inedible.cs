﻿using System;

namespace Shop
{
    public abstract class Inedible : Product
    {
        protected string _material;

        public Inedible()
        {
            _material = "";
            _measure = "шт.";
        }

        public override void ShowProductInfo()
        {
            base.ShowProductInfo();
            Console.Write($", материал: {_material}");
        }
    }
}
