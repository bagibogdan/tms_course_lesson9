﻿using System;

namespace Shop
{
    public abstract class Vegetarian : Edible, IVegetarian
    {
        public bool IsVegetarian => true;

        public override void ShowProductInfo()
        {
            base.ShowProductInfo();
            Console.Write(", вегетарианский продукт");
        }
    }
}
