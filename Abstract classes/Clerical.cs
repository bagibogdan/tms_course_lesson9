﻿using System;

namespace Shop
{
    public abstract class Clerical : Inedible, IClerical
    {
        public bool IsClerical => true;

        public override void ShowProductInfo()
        {
            base.ShowProductInfo();
            Console.Write(", товар канцелярский.");
        }
    }
}
